# AKCookingBot

Simple image-macro based cooking bot for Aura Kingdom.

It can be quite particular about your image qualty and image settings in-game, so if it doesn't work, fiddle around with the scan images: `scanimage.png` and `scanimagecomplete.png`.
Note that due to my laziness, the coords for these images are hard-coded, so if they are updated, the updates need to be the exact same size as the originals. Or you can just fix the coords in the code.