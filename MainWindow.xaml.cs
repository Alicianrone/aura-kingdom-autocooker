﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Timers;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Navigation;
using System.Drawing.Imaging;
using System.Windows.Threading;
using System.Threading;
using System.Windows.Forms;

namespace AKCookingBot {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        const double TOLERANCE = 0.25;

        DispatcherTimer screenScanner;
        DispatcherTimer minigameScanner;
        DispatcherTimer gc = new DispatcherTimer(DispatcherPriority.SystemIdle);
        Bitmap scanImage = new Bitmap(Bitmap.FromFile("scanimage.png"));
        Bitmap scanImageComplete = new Bitmap(Bitmap.FromFile("scanimagecomplete.png"));
        Bitmap lastScreenScan;
        Bitmap[] ingredientScans;
        Rectangle lastScreenScanResult;
        Rectangle cookingWindowLocation;
        volatile bool locked = false;
        public MainWindow() {
            InitializeComponent();

            Reset();
            gc.Interval = TimeSpan.FromMilliseconds(8000);
            gc.Tick += new EventHandler(GCTick);
            gc.Start();

            lblStatus.Content = "Scanning for cooking window.";
        }

        private void Reset() {
            screenScanner = new DispatcherTimer(DispatcherPriority.Normal);
            screenScanner.Interval = TimeSpan.FromMilliseconds(1000);
            screenScanner.Tick += new EventHandler(UpdateScreenScan);
            screenScanner.Start();
        }

        private void GCTick(object sender, EventArgs e) {
            GC.Collect();
        }

        private void UpdateScreenScan(object sender, EventArgs e) {
            if (!locked) {
                locked = true;
                lastScreenScan = new Bitmap(SystemInformation.VirtualScreen.Width, SystemInformation.VirtualScreen.Height, PixelFormat.Format32bppArgb);
                using (var g = Graphics.FromImage(lastScreenScan)) {
                    g.CopyFromScreen(SystemInformation.VirtualScreen.Left, SystemInformation.VirtualScreen.Top, 0, 0, lastScreenScan.Size, CopyPixelOperation.SourceCopy);
                }

                lastScreenScanResult = BitmapOperations.SearchBitmap(scanImage, lastScreenScan, TOLERANCE);
                if (lastScreenScanResult.X > 0) {
                    screenScanner.Stop();
                    //Found the scan image! Now we need to grab the ingredients.
                    lblStatus.Content = "Found cooking menu, fetching ingredients.";
                    cookingWindowLocation = new Rectangle(lastScreenScanResult.X - 240, lastScreenScanResult.Y - 25, 770, 270);
                    Thread.Sleep(800);

                    var cookingBmp = lastScreenScan.Clone(cookingWindowLocation, lastScreenScan.PixelFormat);
                    ingredientScans = new Bitmap[3];
                    ingredientScans[0] = cookingBmp.Clone(new Rectangle(207, 73, 32, 32), cookingBmp.PixelFormat);
                    ingredientScans[1] = cookingBmp.Clone(new Rectangle(269, 73, 32, 32), cookingBmp.PixelFormat);
                    ingredientScans[2] = cookingBmp.Clone(new Rectangle(332, 73, 32, 32), cookingBmp.PixelFormat);
                    imgWrong1.Source = BitmapOperations.ToBitmapSource(ingredientScans[0]);
                    imgWrong2.Source = BitmapOperations.ToBitmapSource(ingredientScans[1]);
                    imgWrong3.Source = BitmapOperations.ToBitmapSource(ingredientScans[2]);
                    lblStatus.Content = "Detected, playing cooking minigame.";

                    minigameScanner = new DispatcherTimer(DispatcherPriority.SystemIdle);
                    minigameScanner.Interval = TimeSpan.FromMilliseconds(250);
                    minigameScanner.Tick += new EventHandler(CookingMinigame);
                    minigameScanner.Start();
                }
                locked = false;
            }
        }

        private void CookingMinigame(object sender, EventArgs e) {
            var cookingBmp = new Bitmap(cookingWindowLocation.Width, cookingWindowLocation.Height + 125, PixelFormat.Format32bppArgb);
            using (var g = Graphics.FromImage(cookingBmp)) {
                g.CopyFromScreen(SystemInformation.VirtualScreen.Left + cookingWindowLocation.X, SystemInformation.VirtualScreen.Top + cookingWindowLocation.Y - 125, 0, 0, cookingBmp.Size, CopyPixelOperation.SourceCopy);
            }

            var stopResult = BitmapOperations.SearchBitmap(scanImageComplete, cookingBmp, TOLERANCE);
            if (stopResult.X == 0) {
                minigameScanner.Stop();
                lblStatus.Content = "Finished, waiting for next cook...";
                Reset();
            } else {
                for (var i = 0; i <= 2; i++) {
                    var sResult = BitmapOperations.SearchBitmap(ingredientScans[i], cookingBmp, TOLERANCE);
                    if (sResult.X > 0) {
                        var mouseClickX = SystemInformation.VirtualScreen.Left + cookingWindowLocation.X + sResult.X + 1;
                        var mouseClickY = SystemInformation.VirtualScreen.Top + cookingWindowLocation.Y - 125 + sResult.Y + 22;
                        MouseEvents.LeftMouseClick(mouseClickX, mouseClickY);
                        //TODO Add ESC hotkey
                        return;
                    }
                }
            }
        }

        private void chkAltScan_Checked(object sender, RoutedEventArgs e) {
            scanImage = new Bitmap(Bitmap.FromFile("scanimage_alt.png"));
            scanImageComplete = new Bitmap(Bitmap.FromFile("scanimagecomplete_alt.png"));
        }

        private void chkAltScan_Unchecked(object sender, RoutedEventArgs e) {
            scanImage = new Bitmap(Bitmap.FromFile("scanimage.png"));
            scanImageComplete = new Bitmap(Bitmap.FromFile("scanimagecomplete.png"));
        }
    }
}
